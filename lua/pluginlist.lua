-- nvim/lua/pluginlist.lua

return {

	-- Colorscheme
	{
		'folke/tokyonight.nvim',
		priority = 1000,
		config = function()
			vim.cmd("colorscheme tokyonight-night")
		end
	},

	-- Git related plugins
	'tpope/vim-fugitive',
	'tpope/vim-rhubarb',

    -- Adds git releated signs to the gutter, as well as utilities for managing changes
    'lewis6991/gitsigns.nvim',

    -- autoclose
    'm4xshen/autoclose.nvim',

    -- Undo Tree
    'mbbill/undotree',

    -- VIM BBYE
    'moll/vim-bbye',

    { -- Bar at the top
        'akinsho/bufferline.nvim',
        version = '*',
        dependencies = 'nvim-tree/nvim-web-devicons',
    },

    { -- gc/gcc to comment
        'numToStr/Comment.nvim',
        opts = {},
        lazy = false,
		config = function()
		    require("Comment").setup()
		end
    },

	{ -- Indentation marking
		'lukas-reineke/indent-blankline.nvim',
		main = "ibl",
		opts = {}
	},

	{
        'folke/todo-comments.nvim',
        dependencies = { 'nvim-lua/plenary.nvim' },
        opts = {}
    },

    { -- Highlight, edit, and navigate code
        'nvim-treesitter/nvim-treesitter',
		build = ':TSUpdate',
         dependencies = {
            'nvim-treesitter/nvim-treesitter-textobjects',
            --'nvim-treesitter/nvim-treesitter-context', -- this is the one that gives context
        },
		-- I'd love to know where this is from
        --config = function()
        --    pcall(require('nvim-treesitter.install').update { with_sync = true })
        --end
    },

	{
		'VonHeikemen/lsp-zero.nvim',
		branch = 'v3.x',
		dependencies = {
			-- LSP Support
			{'neovim/nvim-lspconfig'},             -- Required
			{ 'williamboman/mason.nvim'},          -- Optional
			{'williamboman/mason-lspconfig.nvim'}, -- Optional

			-- Autocompletion
			{'hrsh7th/nvim-cmp'}, -- Required
			{'hrsh7th/cmp-nvim-lsp'}, -- Required
			{'hrsh7th/cmp-buffer'}, -- Optional
			{'hrsh7th/cmp-path'}, -- Optional
			{'saadparwaiz1/cmp_luasnip'}, -- Optional
			{'hrsh7th/cmp-nvim-lua'}, -- Optional
			{'L3MON4D3/LuaSnip'},     -- Required -- TODO: make date work again and make cs header
			{'rafamadriz/friendly-snippets'}, -- Optional
			{'folke/neodev.nvim'} -- for neovim information
        }
    }
}
